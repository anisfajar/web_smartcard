<!-- <?php 
//header("Refresh: 1;");
?> -->
<!DOCTYPE html>
<html>
<head>
    <title>WebCard Test Page</title>
</head>
<body>
    <?php
        # Reader Omnikey OMNIKEY CardMan 5x21-CL 0 = Contactless reader
        $context = scard_establish_context(); # ambil context PC/SC
        $readers = scard_list_readers($context);  # ambil reader yang terpasang
        if (!$readers) {
            echo "<b>TIDAK ADA READER</b> <b style='color:red'>OMNIKEY CardMan 5x21-CL 0 </b> <b>YANG TERPASANG !!</b>";
        } else {
            $reader = $readers[1];
            echo "Nama Reader: ", "<b>".$reader."</b>", "\n";
            echo "<p>";
            
            $connection = scard_connect($context, $reader); # Koneksi ke Smartcard
            if ($connection) {
                $CMD = "FFCA000000";
                $res = scard_transmit($connection, $CMD);
                echo "<b style = color:blue; font-family:arial> ID Card : ".substr($res,0,-4)."</b>";
                echo "<p>";
                echo "<b style = color:teal; font-family:arial> Status : ".implode(' ', str_split(substr($res,-4),2))."</b>";
            } else {
                echo "<b style=color:red;font-family:arial> Kartu tidak terdeteksi !!</b>";
            }
            scard_is_valid_context($context);
        }
        scard_release_context($context);  # Release the PC/SC context
    ?>
    <hr>
    <?php
        # Reader YP2137 Contact Reader 0 = Contact reader
        $context = scard_establish_context(); # ambil context PC/SC
        $readers = scard_list_readers($context);  # ambil reader yang terpasang

        if (!$readers) {
            echo "<b>TIDAK ADA READER</b> <b style='color:red'>YP2137</b> <b>YANG TERPASANG !!</b>";
        } else {
            // $reader = $readers[0];
            $reader = $readers[0];
            echo "Nama Reader: ", "<b>".$reader."</b>", "\n";
            echo "<p>";
            $connection = scard_connect($context, $reader); # Koneksi ke Smartcard
            if (!$connection) {
                echo "<b style=color:red;font-family:arial> Gagal terkoneksi kartu !!</b>";
            } else {
                $CMD = "00A404000C504E53436172644A6F676A6100";
                $res = scard_transmit($connection, $CMD);
                $CMD = "F0CA000012";
                $res = scard_transmit($connection, $CMD);

                echo "<b style = color:blue; font-family:arial> Result : ".$res."</b>";
                echo "<br>";
                echo "<p>";
                echo "<br>";
                echo "<b style = color:teal; font-family:arial> Data : ".trim(substr($res,0,-4))."</b>";
                echo "<p>";
                echo "<b style = color:teal; font-family:arial> Status : ".implode(' ', str_split(substr($res,-4),2))."</b>";
                echo "<br>";
                echo "<br>";
                echo "<b style = color:red; font-family:arial> Data (ASCII) : ".hex2bin(trim(substr($res,0,-4)))."</b>";
            }
        }
        scard_release_context($context); # Release the PC/SC context
    ?>

    <hr>
 
    <script type="text/javascript">
        function pluginLoaded() {
            window.webcard = document.getElementById("webcard");
            if (webcard.attachEvent) {
                webcard.attachEvent("oncardpresent", cardPresent);
                webcard.attachEvent("oncardremoved", cardRemoved);
            } else { 
                webcard.addEventListener("cardpresent", cardPresent, false);
                webcard.addEventListener("cardremoved", cardRemoved, false);
            }
        }

        function cardPresent(reader) {
            reader.connect(2); // 1-Exclusive, 2-Shared
            console.log("ATR : " + reader.atr);
            var apdu = "FFCA000000";
            var resp = reader.transcieve(apdu);
            console.log("UID : " + resp);
            reader.disconnect();
        }

        function cardRemoved(reader) {}

    </script>
    
    <object id="webcard" type="application/x-webcard" width="0" height="0">
        <param name="onload" value="pluginLoaded" />
    </object>
</body>
</html>